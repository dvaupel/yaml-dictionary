#!/usr/bin/env python3

import yaml


def main():
	source = 'koreanisch.yaml'
	n_doc = 1   # 1. Vorspann, 2. Inhalt

	with open(source) as f:
		docs = yaml.load_all(f, Loader=yaml.FullLoader)
		for doc in docs:
			if(n_doc == 1):
				process_intro(doc)
			elif (n_doc == 2):
				process_dict(doc)
			else:
				pass

			n_doc += 1

# Metadaten verarbeiten (Überschrift, Author, ...)
def process_intro(doc):
	pass

# meaning kann string oder list sein.
# n ist optional und wird der Bedeutung vorangestellt
def process_meaning(meaning, n: int = None):
	if n is not None:
		print('\\tcbox{{{}}}'.format(n))

	if isinstance(meaning, list):
		print(', '.join(meaning))
	else:
		print(meaning)


def process_dict(doc):
	entries = dict(sorted(doc.items(), key=lambda item: item[0]))

	for kor,deu in entries.items():

		print('{{\\color{{blue}}{} \\markboth{{{}}}{{{}}}}}'.format(kor, kor, kor)) # Koreanisch

		
		if 'laut' in deu:
			print('{{\\color{{blue}} \\small [{}]}}'.format(deu['laut']))           # Aussprache (optional)

		
		bedeutungen: list = deu['bed']                                              # Bedeutung(en)
		
		if len(bedeutungen) == 0:
			raise RuntimeError('Eintrag ohne Übersetzung!')
		elif len(bedeutungen) == 1:
			process_meaning(bedeutungen[0])
		else:
			for mean, num in zip(bedeutungen, range(1, len(bedeutungen)+1)):
				process_meaning(mean, num)

		print('')
		print('\\medskip')
		print('')



if __name__ == '__main__':
	main()
